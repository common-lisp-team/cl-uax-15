(require "asdf")

; Until the testsuite is really exercised (would require “parachute” to be
; packaged), the tests are marked “superficial” in debian/tests/control

(let ((asdf:*user-cache* (uiop:getenv "AUTOPKGTEST_TMP"))) ; Store FASL in some temporary dir
  (asdf:load-system "uax-15"))
